<?php

namespace Drupal\sectorc_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Example: empty block' block.
 *
 * @Block(
 *   id = "our_work_filters_block",
 *   admin_label = @Translation("Our Work block filters")
 * )
 */
class WorkBlock extends BlockBase {

  public function build() {
    // We return an empty array on purpose. The block will thus not be rendered
    // on the site. See BlockExampleTest::testBlockExampleBasic().
      $terms=\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadtree('work_categories');
      $filters=array();
      foreach ($terms as $term) {
          $filters[$term->tid]['name'] = $term->name;
          $filters[$term->tid]['tid'] = $term->tid;
      }

      return array(
          '#theme' => 'sectorc_core_work_filters',
          '#filters' => $filters,
      );
  }

}
