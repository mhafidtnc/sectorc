<?php

namespace Drupal\sectorc_core\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;


/**
 * Controller routines for AJAX example routes.
 */
class SectorcController extends ControllerBase {


  public function getwork($category) {

      $results=views_get_view_result('our_work', 'our_work_block',$category);
      $nids=array();
      foreach ($results as $result) {
          $nids[]=$result->nid;
      }

      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);
      $items=array();

      foreach ($nodes as $node) {
          $nid=$node->id();
          $items[$nid]['client']=$node->get('field_client')->value;
          if ($node->get('field_image')->entity!=NULL){
              $items[$nid]['uri']=$node->get('field_image')->entity->getFileUri();
          }
      }

       $output = array
       (
           '#theme' => 'sectorc_core_work_block',
           '#items' => $items,
       );

      $response = new AjaxResponse();
      $response->addCommand(new ReplaceCommand('#our-work-block', $output));


      return $response;

  }

}
